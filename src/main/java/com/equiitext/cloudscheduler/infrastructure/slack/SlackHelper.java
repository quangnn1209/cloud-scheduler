package com.equiitext.cloudscheduler.infrastructure.slack;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.gson.GsonFactory;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SlackHelper {
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), new ThreadFactoryBuilder().setNameFormat("[userId=admin-" + Math.random() + "]").build());

    private static final Logger LOG = LoggerFactory.getLogger(SlackHelper.class);
    public static void sendException(SlackChannel channel, String message, String stackTrace) {
        EXECUTOR.submit(() -> {
            try {
                ImmutableMap.Builder<Object, Object> params = ImmutableMap.builder()
                        .put("text", message)
                        .put("username", "Cloud Pay [Server " + InetAddress.getLocalHost().getHostName() + "]")
                        .put("icon_url", "https://pbs.twimg.com/profile_images/422780584113356800/rCnDzQ_g_400x400.jpeg");
                if (StringUtils.isNotBlank(stackTrace))
                    params = params.put("attachments", new Map[]{ImmutableMap.of("text", stackTrace)});
                JsonHttpContent content = new JsonHttpContent(new GsonFactory(), params.build());
                new NetHttpTransport().createRequestFactory().buildPostRequest(new GenericUrl(channel.getChannel()), content).execute();
            } catch (Exception ex) {
                LOG.warn("Can not send message to SLACK", ex);
                return false;
            }
            return true;
        });
    }
}
