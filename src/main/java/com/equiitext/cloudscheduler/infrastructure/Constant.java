package com.equiitext.cloudscheduler.infrastructure;

public class Constant {
    public static boolean active = true;

    public static class SchedulerExpr {
        public final static String SCHEDULED_FOLLOWUP_SCHEDULER = "0 */1 * * * *";
        public final static String CHECK_AND_UPDATE_MONTHOVER = "0 0 * * * *";
        public final static String RECOVER_PROCESS = "0 */30 * * * *";
        public final static String NOTIFY_SCHEDULER = "0 */1 * * * *";
        public final static String BACKUP_SCHEDULER = "0 0 7 1/1 * ?";
        public final static String DAILY_FOLLOWUP_SCHEDULER = "0 */12 * * * *";
        public final static String EMAIL_NOTIFICATION_SCHEDULER = "0 */6 * * * *";
        public final static String INBOX_ARCHIVE_SCHEDULER = "0 0 7 1/1 * ?";
        public final static String OUT_OF_CREDIT_SCHEDULER =  "0 30 8 ? * TUE";
        public final static String PHONE_CLEAN_UP = "0 0 6 * * *";
        public final static String REPORT_PREPARING_DATA = "0 */2 * * * *";
        public final static String REPORT_SCHEDULER = "0 */10 * * * *";
        public final static String TIMED_FOLLOWUP_SCHEDULER = "0 */1 * * * *";
        public final static String VIRTUAL_NUMBER_DEDUCTION_SCHEDULER = "0 30 8 1/1 * ?";
        public final static String DELETE_CAMPAIGN_CLEANER = "0 0 1 * * SAT";
        public final static String RECOVER_DEAD_CAMPAIGN_SCHEDULER = "0 */30 * * * *";
        public final static String RESUME_PAUSED = "0 */15 * * * *";
        public final static String TIMED_RESUME_PAUSED = "0 */15 * * * *";
        public final static String USER_STATS_AUTO_SAVE = "0 */20 * * * *";
    }
}
