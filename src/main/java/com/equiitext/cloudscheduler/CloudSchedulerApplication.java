package com.equiitext.cloudscheduler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class CloudSchedulerApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(CloudSchedulerApplication.class, args);
    }
}