package com.equiitext.cloudscheduler.presentation;

import com.equiitext.cloudscheduler.infrastructure.Constant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Quang on 31/10/17.
 */
@RestController
public class CloudSchedulerController {
    @Value("${application.self.key}")
    private String selfKey;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String root() {
        return "Welcome to deep web!";
    }

    @RequestMapping(path = "/turnOff", method = RequestMethod.GET)
    public String turnOff(@RequestParam("key") String key) {
        if (selfKey.equals(key)) {
            Constant.active = false;
        }

        return "OK";
    }

    @RequestMapping(path = "/turnOn", method = RequestMethod.GET)
    public String turnOn(@RequestParam("key") String key) {
        if (selfKey.equals(key)) {
            Constant.active = true;
        }

        return "OK";
    }
}