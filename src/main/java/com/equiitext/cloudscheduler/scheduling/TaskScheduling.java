package com.equiitext.cloudscheduler.scheduling;

import com.equiitext.cloudscheduler.infrastructure.Constant;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@EnableScheduling
@Component
public class TaskScheduling {
    private final static Logger LOGGER = LoggerFactory.getLogger(TaskScheduling.class);
    @Value("${ext.cloud-main.endpoint}")
    private String extEndpoint;
    @Value("${ext.cloud-main.key}")
    private String apikey;

    @Scheduled(cron = Constant.SchedulerExpr.SCHEDULED_FOLLOWUP_SCHEDULER)
    public void scheduledFollowupScheduler() throws IOException {
        if (!Constant.active) {
            return;
        }

        Request request = new Request.Builder()
                .url(extEndpoint + "/scheduledFollowupScheduler?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("scheduledFollowupScheduler: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.CHECK_AND_UPDATE_MONTHOVER)
    public void checkAndUpdateMonthOver() throws IOException {
        if (!Constant.active) {
            return;
        }

        Request request = new Request.Builder()
                .url(extEndpoint + "/renewalSubscriptionPlanUpdater?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("renewalSubscriptionPlanUpdater: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.RECOVER_PROCESS)
    public void recoverProcess() throws IOException {
        if (!Constant.active) {
            return;
        }

        Request request = new Request.Builder()
                .url(extEndpoint + "/recoverProcess?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("recoverProcess: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.NOTIFY_SCHEDULER)
    public void notifyScheduler() throws IOException {
        if (!Constant.active) {
            return;
        }

        Request request = new Request.Builder()
                .url(extEndpoint + "/notifyScheduler?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("notifyScheduler: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.BACKUP_SCHEDULER) // Everyday at 07:00 GMT = midnight PDT
    public void backupScheduler() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/backupScheduler?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("backupScheduler: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.DAILY_FOLLOWUP_SCHEDULER)
    public void dailyFollowupScheduler() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/dailyFollowupScheduler?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("dailyFollowupScheduler: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.EMAIL_NOTIFICATION_SCHEDULER)
    public void emailNotificationScheduler() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/emailNotificationScheduler?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("emailNotificationScheduler: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.INBOX_ARCHIVE_SCHEDULER) // Everyday at 07:00 GMT = midnight PDT
    public void inboxArchiveScheduler() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/inboxArchiveScheduler?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("inboxArchiveScheduler: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.OUT_OF_CREDIT_SCHEDULER) // Every Tuesday at 8:30am GMT = 1:30am PDT
    public void outofCreditScheduler() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/outofCreditScheduler?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("outofCreditScheduler: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.PHONE_CLEAN_UP)//1p GMT+7, 2a EST
    public void phoneCleanUpTask() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/phoneCleanUpTask?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("phoneCleanUpTask: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.REPORT_PREPARING_DATA)
    public void reportPrepareData() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/reportPrepareData?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("reportPrepareData: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.REPORT_SCHEDULER)
    public void reportScheduler() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/reportScheduler?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("reportScheduler: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.TIMED_FOLLOWUP_SCHEDULER)
    public void timedFollowupScheduler() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/timedFollowupScheduler?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("timedFollowupScheduler: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.VIRTUAL_NUMBER_DEDUCTION_SCHEDULER) // Everyday at 08:30am GMT = 1:30am PDT
    public void virtualNumerDeductionScheduler() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/virtualNumerDeductionScheduler?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("virtualNumerDeductionScheduler: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.DELETE_CAMPAIGN_CLEANER)
    public void deletedCampaignCleaner() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/deletedCampaignCleaner?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("deletedCampaignCleaner: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.RECOVER_DEAD_CAMPAIGN_SCHEDULER)
    public void recoverDeadCampaign() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/recoverDeadCampaign?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("recoverDeadCampaign: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.RESUME_PAUSED)
    public void resumePaused() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/resumePaused?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("resumePaused: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.TIMED_RESUME_PAUSED)
    public void timeResumePaused() throws IOException {
        if (!Constant.active) {
            return;
        }
        Request request = new Request.Builder()
                .url(extEndpoint + "/timeResumePaused?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("timeResumePaused: " + response.code());
        response.close();
    }

    @Scheduled(cron = Constant.SchedulerExpr.USER_STATS_AUTO_SAVE)
    public void userStatsAutoSave() throws IOException {
        if (!Constant.active) {
            return;
        }

        Request request = new Request.Builder()
                .url(extEndpoint + "/userStatsAutoSave?key=" + apikey)
                .get()
                .build();

        Response response = new OkHttpClient().newCall(request).execute();
        LOGGER.info("userStatsAutoSave: " + response.code());
        response.close();
    }
}
