package com.equiitext.cloudscheduler;

import com.equiitext.cloudscheduler.scheduling.TaskScheduling;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class TaskSchedulingTest {
    @Autowired
    TaskScheduling taskScheduling;

    @Test
    public void overallTest() throws IOException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(19);
        final List<Callable<Void>> callables = new ArrayList<>();

        callables.add(() -> {
            taskScheduling.backupScheduler();
            return null;
        });
        callables.add(() -> {
            taskScheduling.checkAndUpdateMonthOver();
            return null;
        });
        callables.add(() -> {
            taskScheduling.dailyFollowupScheduler();
            return null;
        });
        callables.add(() -> {
            taskScheduling.deletedCampaignCleaner();
            return null;
        });
        callables.add(() -> {
            taskScheduling.emailNotificationScheduler();
            return null;
        });
        callables.add(() -> {
            taskScheduling.inboxArchiveScheduler();
            return null;
        });
        callables.add(() -> {
            taskScheduling.notifyScheduler();
            return null;
        });
        callables.add(() -> {
            taskScheduling.outofCreditScheduler();
            return null;
        });
        callables.add(() -> {
            taskScheduling.phoneCleanUpTask();
            return null;
        });
        callables.add(() -> {
            taskScheduling.recoverDeadCampaign();
            return null;
        });
        callables.add(() -> {
            taskScheduling.recoverProcess();
            return null;
        });
        callables.add(() -> {
            taskScheduling.resumePaused();
            return null;
        });
        callables.add(() -> {
            taskScheduling.scheduledFollowupScheduler();
            return null;
        });
        callables.add(() -> {
            taskScheduling.timedFollowupScheduler();
            return null;
        });
        callables.add(() -> {
            taskScheduling.timeResumePaused();
            return null;
        });
        callables.add(() -> {
            taskScheduling.userStatsAutoSave();
            return null;
        });
        callables.add(() -> {
            taskScheduling.virtualNumerDeductionScheduler();
            return null;
        });
        callables.add(() -> {
            taskScheduling.reportPrepareData();
            return null;
        });
        callables.add(() -> {
            taskScheduling.reportScheduler();
            return null;
        });
        executor.invokeAll(callables);
    }
}
