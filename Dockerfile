FROM java:8-jdk

MAINTAINER Quang Nguyen <quangnn1209@gmail.com>

ADD build/libs/*.jar /app/
ADD profiler/profiler_java_agent.so /app/profiler/

EXPOSE 8888

VOLUME /log/

ENTRYPOINT java -agentpath:/app/profiler/profiler_java_agent.so=-cprof_service=cloud-scheduler,-cprof_service_version=1.0.0,-cprof_project_id=causal-prism-200821 -jar /app/*.jar -Duser.timezone=America/Los_Angeles